package co.paradaux.Hibernia.PermanentNightVision.timers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

public class PotionEffectRefillTimer {

    public static BukkitTask createTimer(Plugin plugin) {

        return Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () -> {
            Bukkit.getScheduler().runTask(plugin, () -> {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 86400, 1, false, false), true);
                }
            });
        }, 72000, 72000);

    }


}
