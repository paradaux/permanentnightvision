package co.paradaux.Hibernia.PermanentNightVision;

import co.paradaux.Hibernia.PermanentNightVision.events.PlayerJoinEventListener;
import co.paradaux.Hibernia.PermanentNightVision.events.PlayerRespawnEventListener;
import co.paradaux.Hibernia.PermanentNightVision.timers.PotionEffectRefillTimer;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public final class PermanentNightVision extends JavaPlugin {

    BukkitTask refillTimer;

    @Override
    public void onEnable() {

        Bukkit.getPluginManager().registerEvents(new PlayerJoinEventListener(this), this);
        Bukkit.getPluginManager().registerEvents(new PlayerRespawnEventListener(this), this);

        refillTimer = PotionEffectRefillTimer.createTimer(this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
