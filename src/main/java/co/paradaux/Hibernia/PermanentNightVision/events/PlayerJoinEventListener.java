package co.paradaux.Hibernia.PermanentNightVision.events;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerJoinEventListener implements Listener {

    private Plugin plugin;

    public PlayerJoinEventListener(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void giveNightVision(PlayerJoinEvent e) {

        Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
            Bukkit.getScheduler().runTask(plugin, () -> {
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 86400, 1, false, false), true);
            });
        }, 20);

    }
}
